// Calclulator class definition

class Calculator {
  firstNumber = null;
  secondNumber = null;
  operator = null;
  currentNumberElement = null;
  prevoiusNumberElement = null;

  calculate = function () {
    let result;
    switch (this.operator) {
      case "+":
        result = this.firstNumber + this.secondNumber;
        break;

      case "-":
        result = this.firstNumber - this.secondNumber;
        break;

      case "x":
        result = this.firstNumber * this.secondNumber;
        break;

      case "÷":
        result = this.firstNumber / this.secondNumber;
        break;
    }

    this.firstNumber = null;
    this.secondNumber = null;
    this.operator = null;
    return result;
  };
}

//html elements
const clearAllBtnElement = document.querySelector(".js-clear-all-button");

const deleteBtnElement = document.querySelector(".js-delete-button");

const changeSignBtnElement = document.querySelector(".js-change-sign-button");

const numberBtnELements = document.querySelectorAll(".js-number-button");

const operatorBtnElements = document.querySelectorAll(".js-operator-button");

const pointBtnElement = document.querySelector(".js-point-button");

const equalsBtnElement = document.querySelector(".js-equals-button");

const prevoiusNumberElement = document.querySelector(".js-previous-number");

const currentNumberElement = document.querySelector(".js-current-number");

//creating calculator object

const calculator = new Calculator(prevoiusNumberElement, currentNumberElement);

//event handlers
clearAllBtnElement.addEventListener("click", clearAll);

deleteBtnElement.addEventListener("click", deleteLast);

changeSignBtnElement.addEventListener("click", changeSign);

for (const numberBtnElement of numberBtnELements) {
  numberBtnElement.addEventListener("click", handleNumberBtnClick);
}

for (const operatorBtnElement of operatorBtnElements) {
  operatorBtnElement.addEventListener("click", handleOperatorBtnClick);
}

pointBtnElement.addEventListener("click", handlePointBtnClick);

equalsBtnElement.addEventListener("click", handleEqualsBtnClick);

//event handle functions

function deleteLast() {
  const str = currentNumberElement.innerText;
  const len = str.length;

  currentNumberElement.innerText = str.slice(0, str.length - 1);
}

function clearAll() {
  prevoiusNumberElement.innerText = "";
  currentNumberElement.innerText = "";
  calculator.operator = null;
  calculator.firstNumber = null;
  calculator.secondNumber = null;
}

function changeSign() {
  let number = Number(currentNumberElement.innerText);

  currentNumberElement.innerText = String(-number);
}

function handleNumberBtnClick(event) {
  const numberBtnElement = event.target;

  if (
    currentNumberElement.innerText === "0" ||
    currentNumberElement.innerText === "Error"
  ) {
    currentNumberElement.innerText = numberBtnElement.innerText;
  } else if (calculator.firstNumber === null && calculator.operator !== null) {
    calculator.firstNumber = Number(currentNumberElement.innerText);

    currentNumberElement.innerText = numberBtnElement.innerText;
  } else {
    currentNumberElement.innerText += numberBtnElement.innerText;
  }
}

function handleOperatorBtnClick(event) {
  const operatorBtnElement = event.target;

  if (calculator.operator === null) {
    calculator.operator = operatorBtnElement.innerText;

    calculator.firstNumber = Number(currentNumberElement.innerText);

    prevoiusNumberElement.innerText =
      currentNumberElement.innerText + " " + calculator.operator;

    currentNumberElement.innerText = "";
  }
}

function handleEqualsBtnClick(event) {
  if (
    currentNumberElement.innerText !== null &&
    prevoiusNumberElement !== null
  ) {
    calculator.secondNumber = Number(currentNumberElement.innerText);

    const result = calculator.calculate();

    if (
      typeof result === "number" &&
      Number.isFinite(result) &&
      !Number.isNaN(result)
    ) {
      currentNumberElement.innerText = result;
      prevoiusNumberElement.innerText = "";
    } else {
      currentNumberElement.innerText = "Error";
      prevoiusNumberElement.innerText = "";
    }
  }
}

function handlePointBtnClick(event) {
  //checking if display is empty or if it already contains point

  if (
    currentNumberElement.innerText.includes(".") === false &&
    currentNumberElement.innerText !== "" &&
    currentNumberElement.innerText !== "Error"
  ) {
    currentNumberElement.innerText += ".";
  }
}
